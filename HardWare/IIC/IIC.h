#ifndef __IIC_H
#define __IIC_H
#include "wb32f10x_it.h"

#define SDA_IN  1
#define SDA_OUT 0

#define SLAVEID 0xA0   //从设备地址(本机地址)

#define sI2C_SDA_GPIO GPIOB
#define sI2C_SDA_PIN  GPIO_Pin_0

#define sI2C_SCL_GPIO GPIOB
#define sI2C_SCL_PIN  GPIO_Pin_1

#define sI2C_SCL_RCC_Periph         RCC_APB1Periph_GPIOB   //SCL GPIO时钟
#define sI2C_SDA_RCC_Periph         RCC_APB1Periph_GPIOB   //SDA GPIO时钟

#define sI2C_SCL_PortSource         GPIO_PortSourceGPIOB   //SCL 中断线端口
#define sI2C_SCL_PinSource          GPIO_PinSource1        //SCL 中断线引脚

#define sI2C_SDA_PortSource         GPIO_PortSourceGPIOB   //SDA 中断线端口
#define sI2C_SDA_PinSource          GPIO_PinSource0        //SDA 中断线引脚

#define sI2C_SDA_EXTI_Line          EXTI_Line0             //SDA对应的中断线
#define sI2C_SCL_EXTI_Line          EXTI_Line1             //SCL对应的中断线

#define sI2C_SDA_NVIC_IRQChannel    EXTI0_IRQn             //SDA对应的中断通道
#define sI2C_SCL_NVIC_IRQChannel    EXTI1_IRQn             //SDA对应的中断通道

#define sI2C_GET_SCL() GPIO_ReadInputDataBit(sI2C_SCL_GPIO,sI2C_SCL_PIN)  //获取SCL线状态
#define sI2C_GET_SDA() GPIO_ReadInputDataBit(sI2C_SDA_GPIO,sI2C_SDA_PIN)  //获取SDA线状态

typedef enum
{
    sI2C_STATE_NA = 0,   //初始状态
    sI2C_STATE_STA,      //START
    sI2C_STATE_ADD,      //ADD
    sI2C_STATE_ADD_ACK,  //ADD应答
    sI2C_STATE_DAT,      //数据
    sI2C_STATE_DAT_ACK,  //数据应答
    sI2C_STATE_STO       //STOP
}sI2C_STATE;

                        
extern void IIC_Config(void);
 
#endif

