/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USBD_HW_H
#define __USBD_HW_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "wb32f10x.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

void USBD_HW_ReadEP(uint8_t ep_addr, uint8_t* pbuf, uint16_t size);
void USBD_HW_WriteEP(uint8_t ep_addr, const uint8_t* pdata, uint16_t len);
void USBD_HW_Transmit(uint8_t ep_addr, const uint8_t* pdata, uint16_t len);
uint16_t USBD_HW_GetRxDataCount(uint8_t ep_addr);
void USBD_HW_ReadyToReceive(uint8_t ep_addr);
void USBD_HW_SetStallEP(uint8_t ep_addr);
void USBD_HW_ClrStallEP(uint8_t ep_addr);
uint8_t USBD_HW_IsStalled(uint8_t ep_addr);


#ifdef __cplusplus
}
#endif

#endif /* __USBD_HW_H */
